const exp = require('express');
var student = exp.Router();
//importing dbconfig file
const initDb = require('../DBconfig').initDb;
const getDb = require('../DBconfig').getDb;
initDb();
student.post('/register',(req,res,next)=>{
    dbo= getDb();
    if(req.body=={}){
        res.json({message:'server did not receive data'})
    }else{
        dbo.collection('register').insertOne(req.body,(err,dataArray)=>{
            if(err){
                next();
            }else{
                res.json({message:'register successully'})
            }
        })
    }
})
student.post('/login',(req,res,next)=>{
    dbo=getDb();
    dbo.collection('register').find({email:{$eq:req.body.email}} && {password:{$eq:req.body.password}}).toArray((err,useerArray)=>{
        if(useerArray.length==0)
        {
            res.json({message:'enter valid user and password'})
        }
        else{
            res.json({message:"login success"})
        }
    })
});

student.get('/view', (req, res, next) => {
    dbo = getDb();
    dbo.collection('register').find().toArray((err, dataArray) => {
        if (err) {
            console.log("error in save operation");
            next(err);
        }
        else {
            res.json({ "message": dataArray })
        }
    });
});
student.put('/update',(req,res,next)=>{
    dbo=getDb();
    dbo.collection('register').updateOne({email:{$eq:req.body.email}}, {$set:{name:req.body.name,mname:req.body.mname,lname:req.body.lname,number:req.body.number}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else{
            res.json({message:'successfully updated'})
        }
    })
})
student.delete('/delete/:email',(req,res,next)=>{
    dbo=getDb();
    dbo.collection('register').deleteOne({email:{$eq: req.params.email}},(err,success)=>{
        if(err)
        {
            next(err)
        }
        else{
            dbo.collection('register').find().toArray((err,dataArray)=>{
                if(err)
                {
                    next(err)
                }
                else{
                    res.json({message:'record delete',data:dataArray})
                }
            })
        }
    })
})




student.use((err, req, res, next) => {
    console.log(err);
})
module.exports = student;
