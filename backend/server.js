const exp = require('express');
const app = exp();
const path = require('path');
const initDb=require('./DBconfig').initDb
const getDb=require('./DBconfig').getDb
var dbo=initDb();
//importing angular app with server
app.use(exp.static(path.join(__dirname, '../dist/task/')));
const bodyparser = require('body-parser');
app.use(bodyparser.json());

const student = require('./routes/student');
app.use('/student', student);



const port=4500;
app.listen(port,()=>{
    console.log('server running in port...')
})