import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit() {
  }
Change(x){
  console.log(x);
  this.http.post('/student/register',x).subscribe((res)=>{
    alert(res['message'])
    this.router.navigate(['/login']);
  })
}
}
