import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private http:HttpClient, private router:Router) { }

  ngOnInit() {
  }
  method(x){
    console.log(x);
    
    this.http.post('/student/login',x).subscribe((res)=>{
      if(res['message']=='enter valid user and password'){
      alert(res['message'])
      }
      else{
        alert('login success')
        this.router.navigate(['/view'])
      }
    })
 }
  }

