import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
users:any;
objectmodify:object;
b:boolean=false;
data:any;
  constructor(private http:HttpClient) { }

  ngOnInit() {
    this.http.get('/student/view').subscribe(res=>{
      this.users=res['message']
    })
  }
  edit(user){
    this.objectmodify = user;
    this.b=true;
  }
  change(x){
    this.b=false;
    this.http.put('/student/update', x).subscribe((res)=>{
      alert(res['message'])
    })
  }
delete(email){
  this.http.delete(`/student/delete/${email}`).subscribe(res=>{
    alert(res['message']);
    this.users=res['data']
  })
}
}